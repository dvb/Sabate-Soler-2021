Contact info: jens.schwamborn@uni.lu or sonia.sabatesoler@uni.lu

Please cite: https://doi.org/10.1002/glia.24167 

Complete dataset can be found here: https://doi.org/10.17881/cx25-ht49

Script details:
- Marker analysis: The scripts in this folder were used for Fig. 2B, Fig. 3E and Fig. S1D.
- Cytokines: The scripts in this folder were used for Fig. 3A.
- Medium optimization: The scripts in this folder were used for Fig. 1G-H.
- Metabolomics: The scripts in this folder were used for Fig. S6F.
- snRNA-seq: The scripts in this folder were used for Fig. 2D-G, Fig. 3C, Fig. 4, Fig. 5A-B, Fig. S2, Fig. S3, Fig. S4, Fig. S5 and Fig. S6A-C.
